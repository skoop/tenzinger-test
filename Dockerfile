FROM dunglas/frankenphp

ENV SERVER_NAME=:1080

RUN apt-get update \
 && apt-get install --yes git-core

RUN install-php-extensions \
    opcache \
    intl \
    zip

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

ARG USER=root

# RUN adduser ${USER}
	# Caddy requires an additional capability to bind to port 80 and 443
RUN setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/frankenphp
	# Caddy requires write access to /data/caddy and /config/caddy
RUN chown -R ${USER}:${USER} /data/caddy && chown -R ${USER}:${USER} /config/caddy

RUN chown -R ${USER}:${USER} /app

USER ${USER}

COPY . /app

RUN composer install --optimize-autoloader
