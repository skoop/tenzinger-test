<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Employee;

class EmployeeRepository
{
    private $employees = [
        [
            'name' => 'Paul',
            'transport' => 'car',
            'distance' => 60,
            'workdays' => 5,
        ],
        [
            'name' => 'Martin',
            'transport' => 'bus',
            'distance' => 8,
            'workdays' => 4,
        ],
        [
            'name' => 'Jeroen',
            'transport' => 'bike',
            'distance' => 9,
            'workdays' => 5,
        ],
        [
            'name' => 'Tineke',
            'transport' => 'bike',
            'distance' => 4,
            'workdays' => 3,
        ],
        [
            'name' => 'Arnout',
            'transport' => 'train',
            'distance' => 23,
            'workdays' => 5,
        ],
        [
            'name' => 'Matthijs',
            'transport' => 'bike',
            'distance' => 11,
            'workdays' => 4.5,
        ],
        [
            'name' => 'Rens',
            'transport' => 'car',
            'distance' => 12,
            'workdays' => 5,
        ],
    ];
    public function getAllEmployees(): array
    {
        $list = [];

        foreach ($this->employees as $employee) {
            $list[] = new Employee($employee['name'], $employee['transport'], (int) ($employee['distance']*100), (int) ($employee['workdays']*100));
        }
        return $list;
    }
}