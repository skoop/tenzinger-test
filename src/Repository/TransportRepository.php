<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Transport;
use InvalidArgumentException;

class TransportRepository
{
    private array $transports = [
        [
            'id' => 'bus',
            'label' => 'Bus',
            'base_compensation' => 25,
        ],
        [
            'id' => 'car',
            'label' => 'Car',
            'base_compensation' => 10,
        ],
        [
            'id' => 'bike',
            'label' => 'Bike',
            'base_compensation' => 50,
        ],
        [
            'id' => 'train',
            'label' => 'Train',
            'base_compensation' => 25,
        ],
    ];

    public function getTransportById(string $id): Transport
    {
        foreach ($this->transports as $transport)
        {
            if ($transport['id'] === $id) {
                return new Transport($transport['id'], $transport['label'], $transport['base_compensation']);
            }
        }

        throw new InvalidArgumentException('Unknown transport ' . $id);
    }
}