<?php

declare(strict_types=1);

namespace App\Entity;

class Transport
{
    public function __construct(
        private string $id,
        private string $label,
        private int $baseCompensation
    ) {

    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getBaseCompensation(): int
    {
        return $this->baseCompensation;
    }
}