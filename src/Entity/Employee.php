<?php

declare(strict_types=1);

namespace App\Entity;

class Employee
{
    public function __construct(
        private string $name,
        private string $transport,
        private int $distance,
        private int $workdays
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTransport(): string
    {
        return $this->transport;
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    public function getWorkdays(): int
    {
        return $this->workdays;
    }
}