<?php

declare(strict_types=1);

namespace App\Compensation;

use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use App\Repository\TransportRepository;
use DateInterval;
use DateTimeImmutable;

class CompensationCalculator
{
    private array $workDays = [1, 2, 3, 4, 5];

    public function __construct(
        private EmployeeRepository $employeeRepository,
        private TransportRepository $transportRepository)
    {
    }

    public function calculateCompensation()
    {
        $compensatableDays = [];

        $date = new DateTimeImmutable('first day of january this year');
        $today = new DateTimeImmutable('today');

        while ($date->format('Y-m-d') !== $today->format('Y-m-d')) {
            if (in_array($date->format('w'), $this->workDays)) {
                if (!isset($compensatableDays[$date->format('m')])) {
                    $compensatableDays[$date->format('m')] = [];
                }
                if (!isset($compensatableDays[$date->format('m')][$date->format('w')])) {
                    $compensatableDays[$date->format('m')][$date->format('w')] = 0;
                }

                $compensatableDays[$date->format('m')][$date->format('w')]++;
            }

            $date = $date->add(DateInterval::createFromDateString('1 day'));
        }
        
        $compensations = [];

        $employees = $this->employeeRepository->getAllEmployees();

        /** @var Employee $employee */
        foreach ($employees as $employee) {
            $compensations[$employee->getName()] = [];

            $numberOfDaysPerWeekTraveled = round($employee->getWorkdays()/100, 0, PHP_ROUND_HALF_UP);

            foreach ($compensatableDays as $month => $monthlyData) {
                $paymentDate = new DateTimeImmutable($today->format('Y').'-' . $month . '-01');
                $paymentDate = $paymentDate->modify('first monday of next month');

                $usedTransport = $this->transportRepository->getTransportById($employee->getTransport());

                $compensations[$employee->getName()][$month] = [
                    'employee' => $employee->getName(),
                    'transport' => $usedTransport->getLabel(),
                    'traveled_distance' => 0,
                    'compensation' => 0,
                    'payment_date' => $paymentDate->format('Y-m-d'),
                ];

                $compensatedDays = 0;
                foreach ($monthlyData as $day => $amount) {
                    if ($day <= $numberOfDaysPerWeekTraveled) {
                        $compensatedDays = $compensatedDays + $amount;
                    }
                }

                $compensations[$employee->getName()][$month]['traveled_distance'] = ($compensatedDays * ($employee->getDistance() * 2)) / 100;
                if ($employee->getDistance() >= 500 && $employee->getDistance() <= 1000) {
                    $compensations[$employee->getName()][$month]['compensation'] = (($compensatedDays * (($employee->getDistance()*2)*($usedTransport->getBaseCompensation()*2)))/100)/100;
                } else {
                    $compensations[$employee->getName()][$month]['compensation'] = (($compensatedDays * (($employee->getDistance()*2)*$usedTransport->getBaseCompensation()))/100)/100;
                }

            }
        }

        return $compensations;
    }
}