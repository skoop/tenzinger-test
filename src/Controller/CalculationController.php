<?php

namespace App\Controller;

use App\Compensation\CompensationCalculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CalculationController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('calculation/index.html.twig');
    }

    #[Route('/csv', name: 'csv')]
    public function csv(CompensationCalculator $calculator): Response
    {
        $compensations = $calculator->calculateCompensation();

        $csv = '';
        foreach ($compensations as $employee => $month) {
            foreach($month as $number => $compensationLine) {
                $csv .= $number.';'.implode(';', $compensationLine) . "\n";
            }
        }

        $response = new Response($csv);

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'compensations.csv'
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;

    }
}
