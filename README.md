Tenzinger programming test
==========================

This repository contains the code I wrote for the Tenzinger programming 
test.

## Run the code

To run the code, you have to have Docker installed. Clone the repository
and run `docker compose up -d`. This should build the container. Once it 
is running, it should be reachable at 
[localhost:1080](http://localhost:1080).

## Basic setup

The setup is a basic Symfony application. Given this is a basic test I have
not bothered to set up an actual database. Instead, I have repositories
returning data from hardcoded arrays. I have used "entities" to mimic a 
Doctrine setup. 

### Assumptions

I had to make some assumptions based on the assignment. 

My first assumption as that people not working 5 days always have their
(half) days off at the end of the week.

My second assumption was that the assignment listed double compensation for
distances between 5 and 10 kilometers, meaning Tineke, who only travels 4,
does not get double compensation. This assumption also implies that the doubling
of the compensation is based one one-way distance, not total distance for
a day.

Assumption 3 is the fact that we want to calculate until for all days until
today because this will be calculated on actually traveled days and we
don't know yet for sure when someone will travel in the future. Part of this
assumption is, though, that they have traveled all the days that they
were supposed to work. No remote working is supported in this version.


## History

I like to start simple then only add things that are needed. I've
tried to make the Git history reflect exactly that. Aside from looking
at the actual code, it might be useful to see the steps to get to that
code by inspecting the history.

## What could be better

There are definitely things I think can be improved. First of all I only created
a basic test for the most important class (`CompensationCalculator`). There
could definitely be more.

Then the CompensationCalculator: It is way too big, and I would divide that
up more. One idea would be to create seperate classes for the different calculations
that currently all happen in various foreach/while loops. Then having classes to
represent a single compensation/month would be good. A seperate CSV generator
would also remove some logic from the controller: The controller should not
contain logic.

Not logic-related but code-related: I would add Gitlab pipelines to automatically
run tests and check for code style and static analysis violations. I could
even set up file watchers in my IDE that automatically run PHP-CS-Fixer and
PHPStan so that it should (in theory) not be possible to even commit CS and
static analysis violations. The pipeline checks would just be "to be sure" 
in that case.