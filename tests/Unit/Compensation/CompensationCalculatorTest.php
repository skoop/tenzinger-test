<?php

declare(strict_types=1);

namespace Unit\Compensation;

use App\Compensation\CompensationCalculator;
use App\Repository\EmployeeRepository;
use App\Repository\TransportRepository;
use PHPUnit\Framework\TestCase;

class CompensationCalculatorTest extends TestCase
{
    public function testCorrectCalculations()
    {
        // Using the "real" repositories here since they are already hardcoded
        $calculator = new CompensationCalculator(
            new EmployeeRepository(),
            new TransportRepository(),
        );

        $result = $calculator->calculateCompensation();

        // let's assert a "random" set of data

        // Paul, by car (10 cent compensation), January has 23 workdays based on 5 workdays per week
        // = 2760km = 276 compensation
        // First Monday in February 2024 = 5th
        $this->assertSame(2760, $result['Paul']['01']['traveled_distance']);
        $this->assertSame(276, $result['Paul']['01']['compensation']);
        $this->assertSame('2024-02-05', $result['Paul']['01']['payment_date']);

        // Tineke, by bike (50 cent compensation), February has 12 workdays based on 3 days per week
        // = 96km = 48 compensation
        // First Monday in March 2024 = 4th
        $this->assertSame(96, $result['Tineke']['02']['traveled_distance']);
        $this->assertSame(48, $result['Tineke']['02']['compensation']);
        $this->assertSame('2024-03-04', $result['Tineke']['02']['payment_date']);

        // Jeroen, by bike (50 cent compensation doubled for being 5-10km),
        // February has 21 workdays based on 5 days per week
        // = 378km = 378 compensation
        // First Monday in March 2024 = 4th
        $this->assertSame(378, $result['Jeroen']['02']['traveled_distance']);
        $this->assertSame(378, $result['Jeroen']['02']['compensation']);
        $this->assertSame('2024-03-04', $result['Jeroen']['02']['payment_date']);

        // Matthijs, by bike (50 cent compensation),
        // February has 21 workdays based on 4,5 days per week (the ,5 day still means travel that day)
        // = 462km = 231 compensation
        // First Monday in March 2024 = 4th
        $this->assertSame(462, $result['Matthijs']['02']['traveled_distance']);
        $this->assertSame(231, $result['Matthijs']['02']['compensation']);
        $this->assertSame('2024-03-04', $result['Matthijs']['02']['payment_date']);
    }
}